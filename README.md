## StP Menu

This is a faithful re-creation of the main menu of [this game's](https://store.steampowered.com/app/487500/fault__SILENCE_THE_PEDANT/) demo version. The main menu's uniqueness is the fact that the background changes with the seasons and time (i.e. day and night and the time between). And it's all been replicated in my version!

### Demo

A web demo can be found [here](https://quantumhu.com/stpmenu).

This code is sort of old. At the time, no user interaction was required to play the background music, but now you have to click anywhere within the first second of loading the page for the music to play.

### Notes

I'm only uploading the code necessary for this re-creation to work (i.e. the hard part). I ripped the assets from the demo version using RPA Extractor.