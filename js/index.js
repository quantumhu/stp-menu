var d = new Date();
var month = d.getMonth();
var day = d.getDate();
var time = [d.getHours(), d.getMinutes(), d.getSeconds()];

// show the right picture for season
var season = determineSeason(month, day);

// calculate how much time has passed from morning
var limit = new Date();
limit.setHours(7);
limit.setMinutes(0);
limit.setSeconds(0);

var timeOffset = d - limit;
timeOffset = Math.round(timeOffset / 1000);
if (timeOffset < 0) {
  timeOffset += 86400;
}

var bg = document.getElementsByClassName("bg");
var mid = document.getElementsByClassName("mid");
var frontl = document.getElementsByClassName("frontl");
var frontr = document.getElementsByClassName("frontr");
var layers = document.getElementsByClassName("layer");

var inputbox = document.getElementById("inputbox");

// set up all the images
var prefix = "assets/ritfrontyard/";

for (var i = 0; i < bg.length; i++) {
  if (i == 2 && season == "summer") {
    // thanks @munisix
    // naming scheme broken
  	bg[i].src = prefix + "bg summer night.png";
  } else {
  	bg[i].src = prefix + "bg " + season + " " + returnToD(i) + ".jpg";
  }
}

for (var j = 0; j < mid.length; j++) {
  mid[j].src = prefix + "mid " + season + " " + returnToD(j) + ".png";
}

for (var k = 0; k < frontl.length; k++) {
  if (k == 1 && season == "winter") {
    frontl[k].src = prefix + "frontl winter afternoon.png";
  } else {
    frontl[k].src = prefix + "frontl " + season + " " + returnToD(k) + ".png";
  }
}

for (var l = 0; l < frontr.length; l++) {
  frontr[l].src = prefix + "frontr " + season + " " + returnToD(l) + ".png";
}

var scene = document.getElementById("scene");
var parallax = new Parallax(scene);

document.addEventListener("mouseleave", function() {
  // when mouse is no longer on the screen, reset to a specific location
  parallax.disable();
  for (var i = 0; i < layers.length; i++) {
    layers[i].style.transition = "transform 1s";
    layers[i].style.transform = "translate3d(0px, 0px, 0px)";
  }
  setTimeout(function() {
    for (var i = 0; i < layers.length; i++) {
      layers[i].style.transition = "";
    }
  }, 1000);
});

document.addEventListener("mouseenter", function() {
  parallax.enable();
});

document.getElementById("mutebutton").addEventListener("click", function() {
  toggleSound(document.getElementById("mutedis"));
});

var bgMusic;

document.addEventListener("DOMContentLoaded", function(event) {

  setTimeout(function() {
    if (true) {
      bgMusic = new Audio("assets/fault_StP_BGM01_title.ogg");
      bgMusic.volume = 0.5;
      bgMusic.play();
      setInterval(function() {
        bgMusic.pause();
        bgMusic.currentTime = 0;
        bgMusic.play();
      }, 104.5 * 1000);
    }
  }, 3000);
  
  if (true) {
    setTimeout(function() {
      document.getElementById("logo").style.animation = "show 3s ease-in-out forwards";
    }, 100);
  }

  handleFading(timeOffset, function() {
    console.log("finished first iteration");
    setInterval(function() {
  	  handleFading(0);
    }, 86400 * 1000);
  });

  setTimeout(function() {
  	document.getElementById("cover").style.animation = "removecover 1s ease-out forwards";
  }, 100);

});

// FUNCTIONS
function determineToD(timeArray) {
  
  // determine the time of day based on the hour
  if ((timeArray[0] >= 7 && timeArray[1] >= 0 && timeArray[2] >= 0) && (timeArray[0] <= 15 && timeArray[1] <= 59 && timeArray[2] <= 59)) {
  	return "day";
  } else if ((timeArray[0] >= 16 && timeArray[1] >= 0 && timeArray[2] >= 0) && (timeArray[0] <= 20 && timeArray[1] <= 59 && timeArray[2] <= 59)) {
  	return "evening";
  } else if ((timeArray[0] >= 21 && timeArray[1] >= 0 && timeArray[2] >= 0) || (timeArray[0] <= 6 && timeArray[1] <= 59 && timeArray[2] <= 59)) {
  	return "night";
  }

}

function returnToD(num) {
  
  switch (num) {
    case 0:
      return "day";
    case 1:
      return "evening";
    default:
      return "night";
  }

}

function determineSeason(month, day) {

  switch(month) {
  	case 11:
      if (day < 21) {
        return "autumn";
      } else {
        return "winter";
      }
  	case 0:
  	case 1:
  	  return "winter";
  	case 2:
      if (day < 20) {
        return "winter";
      } else {
        return "spring";
      }
  	case 3:
  	case 4:
  	  return "spring";
  	case 5:
      if (day < 21) {
        return "spring";
      } else {
        return "summer";
      }
  	case 6:
  	case 7:
  	  return "summer";
  	case 8:
      if (day < 22) {
        return "summer";
      } else {
        return "autumn";
      }
    case 9:
    case 10:
  	  return "autumn";
  }

}

function handleFading(timeOffset, callback) {

  // the images were created in a way that slowly fading the old image out will create the appearance 
  // of the time changing slowly, which is pretty smart imo

  // at load time, all time of day layers (day, evening, night) are loaded and only the relevant layers are shown
  // then they are slowly faded away, one by one

  var layers_spec;
  
  // figure out which times of day have been completed
  var offset = timeOffset;
  var indOffset = [0, 0, 0];

  if (offset / 32400 >= 1) {
  	offset -= 36000;
  	indOffset[0] = 36000;
  } else {
  	indOffset[0] = offset;
  	offset -= indOffset[0];
  }

  if (offset / 18000 >= 1) {
  	offset -= 14400;
  	indOffset[1] = 14400;
  } else {
  	indOffset[1] = offset;
  	offset -= indOffset[1];
  }

  if (offset / 36000 >= 1) {
  	offset -= 36000;
  	indOffset[2] = 36000;
  } else {
  	indOffset[2] = offset;
  	offset -= indOffset[2];
  }

  layers_spec = document.getElementsByClassName("layer moves lday");
  for (var i = 0; i < layers_spec.length; i++) {
  	layers_spec[i].style.animation = "fadeday 32400s " + indOffset[0] * -1 + "s forwards";
  }
  setTimeout(function() {
  	layers_spec = document.getElementsByClassName("layer moves levening");
  	for (var j = 0; j < layers_spec.length; j++) {
  	  layers_spec[j].style.animation = "fadeevening 18000s " + indOffset[1] * -1 + "s forwards";
  	}
  	setTimeout(function() {
  	  layers_spec = document.getElementsByClassName("layer moves lday");
  	  for (var k = 0; k < layers_spec.length; k++) {
  	  	layers_spec[k].style.animation = "showday 36000s " + indOffset[2] * -1 + "s forwards";
  	  }
  	  setTimeout(function() {
  	  	layers_spec = document.getElementsByClassName("layer moves levening");
  	  	for (var l = 0; l < layers_spec.length; l++) {
  	  	  layers_spec[l].style.animation = "showday 1s 0s forwards";
  	  	}
  	  	setTimeout(function() {
  	  	  layers_spec = document.getElementsByClassName("layer moves");
  	  	  for (var m = 0; m < layers_spec.length; m++) {
  	  	  	layers_spec[m].style.animation = "";
  	  	  }
  	  	  callback();
  	  	}, 100);
  	  }, (36000 - indOffset[2]) * 1000);
  	}, (18000 - indOffset[1]) * 1000);
  }, (32400 - indOffset[0]) * 1000);
}

function toggleSound(img) {
  if (img.src.includes("assets/play.svg")) {
    img.src = "assets/mute.svg";
    bgMusic.volume = 0;
  } else {
    img.src = "assets/play.svg";
    bgMusic.volume = 0.9;
  }
}